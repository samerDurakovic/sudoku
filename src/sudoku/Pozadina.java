package sudoku;

import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public abstract class Pozadina extends JPanel {

	/**
	 * Create the panel.
	 */
	public Pozadina() {

		setLayout(new GridBagLayout());
		
	}

	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		Image pozadina = new ImageIcon(getClass().getResource("/pozadina/pozadina.jpg")).getImage();
		int visina = pozadina.getHeight(this);
		int sirina = pozadina.getWidth(this);
		
		for(int i=0; i<this.getWidth();i+=sirina) {
			for(int j=0;j<this.getHeight();j+=visina) {
				g.drawImage(pozadina, i, j, this);
	}
}
	
}
}
