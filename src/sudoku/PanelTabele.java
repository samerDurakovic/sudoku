package sudoku;

import javax.swing.JPanel;
import java.awt.Dimension;

import javax.imageio.ImageIO;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.GroupLayout.Alignment;
import javax.swing.Icon;
import javax.swing.plaf.basic.BasicToolBarUI.DockingListener;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Rectangle;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

public class PanelTabele extends JPanel {

	/**
	 * Create the panel.
	 */
	protected Matrica m;
	
	private static final Icon unmute = new ImageIcon("unmute.png");
	private static final Icon mute = new ImageIcon("mute.png");
	private static final Icon ceoEkran = new ImageIcon("ceoEkran.png");
	private static final Icon ekran = new ImageIcon("ekran.png");
	private static final Icon kuca = new ImageIcon("kuca.png");
	private static final Icon nazad = new ImageIcon("nazad.png");
	private static final Icon gif = new ImageIcon("bravo.gif");
	private static final Icon gif2 = new ImageIcon("warning.gif");
	
	
	public PanelTabele(Tabela t) {
		 m = new Matrica(t);
		setBackground(Color.WHITE);
		
		JButton btnProveri = new JButton("Proveri");
		btnProveri.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnProveri.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(provera())
					JOptionPane.showMessageDialog(null, "", "Sudoku je ispravno popunjen!", JOptionPane.PLAIN_MESSAGE, gif);
				else
					JOptionPane.showMessageDialog(null, "", "Sudoku nije ispravno popunjen!", JOptionPane.INFORMATION_MESSAGE, gif2);
			}
		});
		
		JButton btnMuzika = new JButton();
		
		podesiIkonicu(Interfejs.frame.melodija, btnMuzika, unmute, mute);
		
		btnMuzika.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(Interfejs.frame.melodija) {
					btnMuzika.setIcon(mute);
					Interfejs.muzika.setVol(0);
					Interfejs.frame.melodija = false;
				}else {
					btnMuzika.setIcon(unmute);
					Interfejs.muzika.setVol(2);
					Interfejs.frame.melodija = true;
				}
				
			}	
		});
		
		podesiDugme(btnMuzika);
	
		
		JButton btnCeoEkran = new JButton(); 
		
		podesiIkonicu(Interfejs.frame.ceoEkran, btnCeoEkran, ekran, ceoEkran);
		
		btnCeoEkran.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Interfejs.frame.ceoEkran();
				if(!Interfejs.frame.ceoEkran)
					btnCeoEkran.setIcon(ceoEkran);
				else
					btnCeoEkran.setIcon(ekran);
			}
		});
		podesiDugme(btnCeoEkran);
		
		JButton btnNazad = new JButton(nazad);
		podesiDugme(btnNazad);
		
		btnNazad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Interfejs.frame.prikazi(Interfejs.ODABIR);
			}
		});
		
		JButton btnGlavniMeni = new JButton(kuca);
		btnGlavniMeni.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Interfejs.frame.prikazi(Interfejs.MAIN_MENU);
			}
		});
		podesiDugme(btnGlavniMeni);
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(844, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(btnProveri, GroupLayout.PREFERRED_SIZE, 139, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(btnCeoEkran, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnMuzika, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnNazad, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnGlavniMeni, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
							.addGap(18)))
					.addGap(20))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
					.addGap(48)
					.addComponent(btnProveri, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnMuzika, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnCeoEkran, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnNazad, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnGlavniMeni, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(245, Short.MAX_VALUE))
		);
		
		Interfejs.frame.podesiProzor(1300, 828);
		add(m);
		setLayout(groupLayout);
		setOpaque(false);
	}
	
	private boolean provera() {
		for(int i = 0;i < 9; i++)
			for(int j = 0;j < 9; j++)
				if(!(m.privremena[i][j].equals(Integer.toString(m.sudoku[i][j])))) {
					return false;
				}
		return true;			
	}
	
	private void podesiDugme(JButton b) {
		
		b.setBackground(Color.WHITE);
		b.setBorderPainted(false);
		b.setContentAreaFilled(false);
		
	}
	
	private void podesiIkonicu(boolean p,JButton b, Icon ikonica1, Icon ikonica2) {
		if(p)
			b.setIcon(ikonica1);
		else
			b.setIcon(ikonica2);
	}
}
