package sudoku;

import java.io.File;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;

public class Muzika {
	
	private Clip klip;

	void pustiMuziku(String lokacija) {
		
		try {
			File lokacijaMuzike = new File(lokacija);
			AudioInputStream audio = AudioSystem.getAudioInputStream(lokacijaMuzike);
			klip = AudioSystem.getClip();
			klip.open(audio);
			klip.start();
			klip.loop(Clip.LOOP_CONTINUOUSLY);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	void setVol(double vol) {
		FloatControl gain = (FloatControl)klip.getControl(FloatControl.Type.MASTER_GAIN);
		float db = (float)(Math.log(vol) / Math.log(10)*20);
		gain.setValue(db);
	}
}
