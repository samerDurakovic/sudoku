package sudoku;

import javax.swing.JPanel;
import javax.swing.JLabel;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JToggleButton;
import javax.swing.JButton;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;


public class OpcijeDugmad extends JPanel {

	/**
	 * Create the panel.
	 */
	
	public OpcijeDugmad() {
		
		JLabel lblMuzika = new JLabel("Muzika:");
		lblMuzika.setFont(new Font("Times New Roman", Font.BOLD, 26));
		
		JToggleButton tgButtonMuzika = new JToggleButton();
		tgButtonMuzika.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		podesiDugme(Interfejs.frame.melodija, tgButtonMuzika, "MUTE", "UNMUTE",false,true);
		
		tgButtonMuzika.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if(tgButtonMuzika.isSelected()) {
					tgButtonMuzika.setText("UNMUTE");
					Interfejs.muzika.setVol(0);
					Interfejs.frame.melodija = false;
				}
				else {
					tgButtonMuzika.setText("MUTE");
					Interfejs.muzika.setVol(2);
					Interfejs.frame.melodija = true;
				}
			}
		});
		
		
		JLabel lblCeoEkran = new JLabel("Ceo ekran:");
		lblCeoEkran.setFont(new Font("Times New Roman", Font.BOLD, 26));
		
	
		JToggleButton tgButtonCeoEkran = new JToggleButton();
		tgButtonCeoEkran.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		podesiDugme(Interfejs.frame.ceoEkran, tgButtonCeoEkran, "OFF", "ON", true, false);

		tgButtonCeoEkran.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Interfejs.frame.ceoEkran();
				if(tgButtonCeoEkran.isSelected()) {
					tgButtonCeoEkran.setText("OFF");
				}
				else {
					tgButtonCeoEkran.setText("ON");
				}
			}
		});
		
		
		
		JButton btnUputstvo = new JButton("Programer");
		btnUputstvo.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnUputstvo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Interfejs.kartice.add(new ProgramerPanel(), Interfejs.PROGRAMER);
				Interfejs.frame.prikazi(Interfejs.PROGRAMER);
			}
		});
		
		JButton btnNazad = new JButton("Nazad");
		btnNazad.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnNazad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Interfejs.frame.prikazi(Interfejs.MAIN_MENU);
			}
		});
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(153)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(tgButtonMuzika, GroupLayout.PREFERRED_SIZE, 295, GroupLayout.PREFERRED_SIZE)
								.addComponent(tgButtonCeoEkran, GroupLayout.PREFERRED_SIZE, 295, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnUputstvo, GroupLayout.PREFERRED_SIZE, 295, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnNazad, GroupLayout.PREFERRED_SIZE, 295, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(251)
							.addComponent(lblMuzika))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(232)
							.addComponent(lblCeoEkran, GroupLayout.PREFERRED_SIZE, 136, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(152, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(19)
					.addComponent(lblMuzika)
					.addGap(18)
					.addComponent(tgButtonMuzika, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(lblCeoEkran, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(tgButtonCeoEkran, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
					.addGap(46)
					.addComponent(btnUputstvo, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
					.addComponent(btnNazad, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE)
					.addGap(32))
		);
		setLayout(groupLayout);
		setOpaque(false);
		
	}
	
	private void podesiDugme(boolean p, JToggleButton tgBtn, String s1, String s2, boolean b1, boolean b2) {
		if(p) {
			tgBtn.setSelected(b1);
			tgBtn.setText(s1);
		}else {
			tgBtn.setSelected(b2);
			tgBtn.setText(s2);
		}
		
	}
}
