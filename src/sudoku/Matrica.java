package sudoku;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.MatteBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

public class Matrica extends JPanel {

	/**
	 * Create the panel.
	 */
	
	protected JTextField[][] tabela = new JTextField[Tabela.vTabele][Tabela.vTabele];
	protected int[][] sudoku;
	protected boolean[][] provera;
	protected String[][] privremena = new String[Tabela.vTabele][Tabela.vTabele];
	
	public Matrica(Tabela t) {

		sudoku = t.getResenja();
		provera = t.getPolja();
		
		setBounds(0, 0, Tabela.duzina, Tabela.sirina);
		setLayout(new GridLayout(Tabela.vTabele, Tabela.vTabele, 0, 0));
		
		for(int red = 0; red < Tabela.vTabele; red++) {
			for(int kolona = 0; kolona< Tabela.vTabele; kolona++) {
				tabela[red][kolona] = new JTextField();
				add(tabela[red][kolona]);
				if (provera[red][kolona]) {
						privremena[red][kolona] = "";
						tabela[red][kolona].setText(""); 
		               tabela[red][kolona].setEditable(true);
		               tabela[red][kolona].setBackground(Tabela.bCelija);
		               tabela[red][kolona].getDocument().addDocumentListener(new NapraviDokumentListener(red,kolona));
		               tabela[red][kolona].addMouseListener(new NapraviMouseListener(red, kolona));
	
		            } else {
		            	privremena[red][kolona] = "" + sudoku[red][kolona];
		               tabela[red][kolona].setText(sudoku[red][kolona] + "");
		               tabela[red][kolona].setEditable(false);
		               tabela[red][kolona].setBackground(Tabela.bCelija);
		               tabela[red][kolona].setForeground(Tabela.textPopCelije);
		            }
				
		            // Uredjivanje celija
		            tabela[red][kolona].setHorizontalAlignment(JTextField.CENTER);
		            tabela[red][kolona].setFont(Tabela.fontBrojeva);
		            tabela[red][kolona].setSize(Tabela.vCelije,Tabela.vCelije);
		            tabela[red][kolona].setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, new Color(0, 0, 0)));
		            if(red == 2 || red == 5)
		            	tabela[red][kolona].setBorder(BorderFactory.createMatteBorder(1, 1, 5, 1, new Color(0, 0, 0)));
		            if(kolona == 2 || kolona == 5)
		            	tabela[red][kolona].setBorder(BorderFactory.createMatteBorder(1, 1, 1, 5, new Color(0,0,0)));
		            if((red == 2 && kolona == 2) || (red == 2 && kolona == 5) || (red == 5 && kolona == 2) || (red == 5 && kolona == 5))
		            	tabela[red][kolona].setBorder(BorderFactory.createMatteBorder(1, 1, 5, 5, new Color(0,0,0)));
				}
		}
}
	
	private class NapraviDokumentListener implements DocumentListener{

		private int red;
		private int kolona;
		
		public NapraviDokumentListener(int red, int kolona) {
			this.red = red;
			this.kolona = kolona;
		}
		
		@Override
		public void insertUpdate(DocumentEvent e) {
			Document izvor = e.getDocument();
			String pokupi = "";
			try {
				pokupi = izvor.getText(0,izvor.getLength());
				privremena[red][kolona] = pokupi;
			} catch (BadLocationException o) {
				// TODO Auto-generated catch block
				o.printStackTrace();
			}
			
			promenaFonta(pokupi);
		}

		@Override
		public void removeUpdate(DocumentEvent e) {
			promenaFonta("");
		}

		@Override
		public void changedUpdate(DocumentEvent e) {
			// TODO Auto-generated method stub
			
		}
		
		private boolean proveriRed(String pokupi) {
			
			for(int k = 0; k<Tabela.vTabele;k++) {
				if(tabela[red][k]==null) continue;
				if(pokupi.equals(tabela[red][k].getText()) && k!=kolona) {
					return true;
				}
			}
			
			return false;
		}
		
		private void podudaranjaRed(String pokupi) {
			
			for(int k = 0; k<Tabela.vTabele;k++) {
				if(tabela[red][k]==null) continue;
				if(pokupi.equals(tabela[red][k].getText()) && k!=kolona) {
					pozadinaPodudaranja(tabela[red][k]);
				}
				else {
					if(k != kolona) tabela[red][k].setBackground(Tabela.sCelija);
				}
			}
			
		}
		
		private boolean proveriKolonu(String pokupi) {

			for(int r = 0; r<Tabela.vTabele;r++) {
				if(tabela[r][kolona]==null) continue;
				if(pokupi.equals(tabela[r][kolona].getText()) && r != red) {
					return true;
				}
			}
	
			return false;
		}
		
		private void podudaranjaKolona(String pokupi) {
			
			for(int r = 0; r<Tabela.vTabele;r++) {
				if(tabela[r][kolona]==null) continue;
				if(pokupi.equals(tabela[r][kolona].getText()) && r != red) {
					pozadinaPodudaranja(tabela[r][kolona]);
				}
				else {
					if(r != red) tabela[r][kolona].setBackground(Tabela.sCelija);
				}
			}
			
		}
		
		private boolean proveriSektor(String pokupi) {
			int r = red - red % 3;
			int k = kolona - kolona % 3;

			for(int i=r;i<r+3;i++)
				for(int j=k;j<k+3;j++)
				{
					if(tabela[i][j]==null) continue;
					if(pokupi.equals(tabela[i][j].getText()) && red != i && kolona != j) 
					return true;
				}
			return false;
		}
		
		private void podudaranjeSektor(String pokupi) {
			int r = red - red % 3;
			int k = kolona - kolona % 3;

			for(int i=r;i<r+3;i++)
				for(int j=k;j<k+3;j++)
				{
					if(tabela[i][j]==null) continue;
					if(pokupi.equals(tabela[i][j].getText()) && red != i && kolona != j) 
					pozadinaPodudaranja(tabela[i][j]);
					else {
						if(red != i && kolona != j) tabela[i][j].setBackground(Tabela.sCelija);
					}
				}
		}
		
		private void podudaranje(String pokupi) {
			podudaranjaRed(pokupi);
			podudaranjaKolona(pokupi);
			podudaranjeSektor(pokupi);
		}
		
		private boolean provereno(String pokupi) {
			return !proveriRed(pokupi) && !proveriKolonu(pokupi) && !proveriSektor(pokupi);
		}
		
		private void promenaFonta(String pokupi) {
			if(!(pokupi.equals(""))){
				try {
					if(!((Integer.parseInt(pokupi) >= 1) && (Integer.parseInt(pokupi) <= 9))) {
						validacijaUnosa();
					}
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					validacijaUnosa();
				} catch (HeadlessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			podudaranje(pokupi);
			
			if(!provereno(pokupi))
				tabela[red][kolona].setForeground(Tabela.textNijeTacno);
			else {
				tabela[red][kolona].setForeground(Tabela.textTacno);
			}
		}

		
		private void validacijaUnosa() {
			
			Runnable doHighlight = new Runnable() {
				
				@Override
				public void run() {
					tabela[red][kolona].setText("");
					JOptionPane.showMessageDialog(null, "Molimo vas da unesete broj od 1 do 9", "GRE�KA", JOptionPane.ERROR_MESSAGE);
				}
			};
			SwingUtilities.invokeLater(doHighlight);
		}
		
		private void pozadinaPodudaranja(JTextField t) {
			
			Runnable doHighlight = new Runnable() {
				
				@Override
				public void run() {
					if(!t.getText().toString().equals(""))
						t.setBackground(Tabela.podudaranje);
				}
			};
			SwingUtilities.invokeLater(doHighlight);
		}
		
		}
	
	private class NapraviMouseListener implements MouseListener{
		
		private int red;
		private int kolona;
		
		public NapraviMouseListener(int red, int kolona) {
			this.red = red;
			this.kolona = kolona;
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			obojiRed(Tabela.sCelija,Tabela.pCelija);
			obojiKolonu(Tabela.sCelija,Tabela.pCelija);
			obojiSektor(Tabela.sCelija,Tabela.pCelija);
			
			tabela[red][kolona].setBackground(Tabela.pCelija);
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			obojiRed(Tabela.bCelija,Tabela.bCelija);
			obojiKolonu(Tabela.bCelija,Tabela.bCelija);
			obojiSektor(Tabela.bCelija,Tabela.bCelija);
		}
		
		private void obojiRed(Color c, Color u) {
			
			for(int k = 0; k<Tabela.vTabele;k++) {
					tabela[red][k].setBackground(c);
			}
			
		}
		
		private void obojiKolonu(Color c, Color u) {
			for(int r = 0; r<Tabela.vTabele;r++) {
				//tabela[red][kolona].setBackground(u);
					tabela[r][kolona].setBackground(c);
				}
		}
		
		private void obojiSektor(Color c, Color u) {
			
			int r = red - red % 3;
			int k = kolona - kolona % 3;

			for(int i=r;i<r+3;i++)
				for(int j=k;j<k+3;j++)
				{
					//tabela[red][kolona].setBackground(u);
					tabela[i][j].setBackground(c);
				}
		}
		
	}
	
		
}
		

