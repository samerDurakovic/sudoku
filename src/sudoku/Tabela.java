package sudoku;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.GenericArrayType;
import java.util.Scanner;

import javax.swing.JTextField;

public class Tabela {
	
	public static final int vTabele = 9;
	public static final int vSektora = 3;
	
	public static final int vCelije = 75;
	public static final int duzina = vCelije * vTabele;
	public static final int sirina = vCelije * vTabele;
	
	public static final Color bCelija = Color.WHITE;
	public static final Color sCelija = new Color(224,224,224);
	public static final Color pCelija = new Color(204,229,255);
	public static final Color textPopCelije = Color.BLACK;
	public static final Color textTacno = Color.BLUE;
	public static final Color textNijeTacno = Color.RED;
	public static final Color podudaranje = new Color(255, 153, 153);
	public static final Font fontBrojeva = new Font("Times", Font.BOLD, 25);
	
	private int [][] resenja; 
	private boolean [][] polja;
	
	private int pocetak;
	
	public Tabela(int pocetak) {
		setPocetak(pocetak);
		
		resenja = citajResenja();
		polja = citajPolja();
	}
	
	
	File datoteka;
	Scanner citaj;
	
	private int [][] citajResenja(){
		datoteka = new File("resenja.txt");
		
		int [][] r = new int[vTabele][vTabele];
		try {
			
			citaj = new Scanner(datoteka);
			int c = 0;
				
			while(c < pocetak) {
				citaj.nextLine();
				c++;
			}
			
			for(int i = 0; i < vTabele; i++)
				for(int j = 0; j< vTabele; j++)
					r[i][j] = citaj.nextInt();
			citaj.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return r;
	}
	
	private boolean [][] citajPolja(){
		
		datoteka = new File("polja.txt");
		
		boolean [][] b = new boolean[vTabele][vTabele];
		try {
			citaj = new Scanner(datoteka);
			
			int c = 0;
			
			while(c < pocetak) {
				citaj.nextLine();
				c++;
			}
			
			for(int i = 0; i < vTabele; i++)
				for(int j = 0; j< vTabele; j++)
					b[i][j] = citaj.nextBoolean();
			citaj.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return b;
	}

	public int[][] getResenja() {
		return resenja;
	}
	
	public boolean[][] getPolja(){
		return polja;
	}
	
	public void setPocetak(int pocetak) {
		this.pocetak = pocetak * vTabele;
}
	
}
