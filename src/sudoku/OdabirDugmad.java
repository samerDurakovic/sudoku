package sudoku;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JFrame;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Toolkit;

import net.miginfocom.swing.MigLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.event.InputMethodListener;
import java.awt.event.InputMethodEvent;
import javax.swing.border.LineBorder;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.event.AncestorListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.AncestorEvent;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import java.awt.Font;

public class OdabirDugmad extends JPanel {
	private JTextField textField;

	/**
	 * Create the panel.
	 */
	
	private Tabela t;
	
	public OdabirDugmad() {
		
		JButton btnSudoku1 = new JButton("Sudoku br.1");
		btnSudoku1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnSudoku1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				formiraj(0);
				Interfejs.frame.prikazi(Interfejs.IGRA);
			}
		});
		
		JButton btnSudoku2 = new JButton("Sudoku br.2");
		btnSudoku2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnSudoku2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				formiraj(1);
				Interfejs.frame.prikazi(Interfejs.IGRA);
			}
		});
		
		JButton btnSudoku3 = new JButton("Sudoku br. 3");
		btnSudoku3.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnSudoku3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				formiraj(2);
				Interfejs.frame.prikazi(Interfejs.IGRA);
			}
		});
		
		JButton btnSudoku4 = new JButton("Sudoku br. 4");
		btnSudoku4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				formiraj(3);
				Interfejs.frame.prikazi(Interfejs.IGRA);
			}
		});
		btnSudoku4.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		JButton btnSudoku5 = new JButton("Sudoku br. 5");
		btnSudoku5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				formiraj(4);
				Interfejs.frame.prikazi(Interfejs.IGRA);
			}
		});
		btnSudoku5.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		JButton btnSudoku6 = new JButton("Sudoku br. 6");
		btnSudoku6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				formiraj(5);
				Interfejs.frame.prikazi(Interfejs.IGRA);
			}
		});
		btnSudoku6.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		JButton btnSudoku7 = new JButton("Sudoku br. 7");
		btnSudoku7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				formiraj(6);
				Interfejs.frame.prikazi(Interfejs.IGRA);
			}
		});
		btnSudoku7.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		JButton btnSudoku8 = new JButton("Sudoku br. 8");
		btnSudoku8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				formiraj(7);
				Interfejs.frame.prikazi(Interfejs.IGRA);
			}
		});
		btnSudoku8.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		JButton btnSudoku9 = new JButton("Sudoku br. 9");
		btnSudoku9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				formiraj(8);
				Interfejs.frame.prikazi(Interfejs.IGRA);
			}
		});
		btnSudoku9.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		JButton btnSudoku10 = new JButton("Sudoku br. 10");
		btnSudoku10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				formiraj(9);
				Interfejs.frame.prikazi(Interfejs.IGRA);
			}
		});
		btnSudoku10.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		JButton btnGlavniMeni = new JButton("Glavni meni");
		btnGlavniMeni.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnGlavniMeni.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Interfejs.frame.prikazi(Interfejs.MAIN_MENU);
			}
		});
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(84)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnSudoku1, GroupLayout.PREFERRED_SIZE, 202, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(btnSudoku2, GroupLayout.PREFERRED_SIZE, 202, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(btnSudoku3, GroupLayout.PREFERRED_SIZE, 202, GroupLayout.PREFERRED_SIZE)
							.addGap(15)
							.addComponent(btnSudoku4, GroupLayout.PREFERRED_SIZE, 202, GroupLayout.PREFERRED_SIZE)
							.addGap(21)
							.addComponent(btnSudoku5, GroupLayout.PREFERRED_SIZE, 202, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnSudoku6, GroupLayout.PREFERRED_SIZE, 202, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(btnSudoku7, GroupLayout.PREFERRED_SIZE, 202, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(btnSudoku8, GroupLayout.PREFERRED_SIZE, 202, GroupLayout.PREFERRED_SIZE)
							.addGap(15)
							.addComponent(btnSudoku9, GroupLayout.PREFERRED_SIZE, 202, GroupLayout.PREFERRED_SIZE)
							.addGap(21)
							.addComponent(btnSudoku10, GroupLayout.PREFERRED_SIZE, 202, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(471)
							.addComponent(btnGlavniMeni, GroupLayout.PREFERRED_SIZE, 147, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(84, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(139)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(btnSudoku1, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnSudoku2, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnSudoku3, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnSudoku4, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnSudoku5, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE))
					.addGap(23)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(btnSudoku6, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnSudoku7, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnSudoku8, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnSudoku9, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnSudoku10, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE))
					.addGap(80)
					.addComponent(btnGlavniMeni, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(138, Short.MAX_VALUE))
		);
		setLayout(groupLayout);
		setOpaque(false);
		
		Interfejs.frame.podesiProzor(1300, 700);
	}

	private void formiraj(int i) {
		t = new Tabela(i);
		Interfejs.kartice.add(new Sudoku(t), Interfejs.IGRA);
	}
}
