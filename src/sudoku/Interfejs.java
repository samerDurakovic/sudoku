package sudoku;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.Toolkit;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.sound.sampled.AudioInputStream;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.CardLayout;
import java.awt.Dimension;

public class Interfejs extends JFrame {

	public static JPanel kartice;
	public static final String MAIN_MENU = "Glavni meni";
	public static final String IGRA = "Igra";
	public static final String ODABIR = "Odabir";
	public static final String OPCIJE = "Opcije";
	public static final String PROGRAMER = "Programer";
	public static final String PUTANJA = "muzika.wav";
	protected static Muzika muzika;
	public static Interfejs frame;
	
	private int prethodniX, prethodniY, prethodnaS, prethodnaV;
	protected boolean ceoEkran = false;
	protected boolean melodija = true;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					frame = new Interfejs();
					frame.setTitle("Sudoku");
					frame.setResizable(false);
					frame.podesiProzor(700, 700);
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Interfejs() {
		
		muzika = new Muzika();
		muzika.pustiMuziku(Interfejs.PUTANJA);
		muzika.setVol(2);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		kartice = new JPanel();
		kartice.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(kartice);
		kartice.setLayout(new CardLayout(0, 0));
		
		kartice.add(new GlavniMeni(),MAIN_MENU);
		
		CardLayout cl = (CardLayout)(kartice.getLayout());
		cl.show(kartice, MAIN_MENU);
	}
	
	protected void podesiProzor(int w, int h) {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setMinimumSize(new Dimension(w,h));
		frame.setLocation(dim.width / 2 - frame.getSize().width / 2, dim.height / 2 - frame.getSize().height / 2);
	}
	
	protected void ceoEkran() {
		if(ceoEkran == false) {
			prethodniX = getX();
			prethodniY = getY();
			prethodnaS = getWidth();
			prethodnaV = getHeight();
			dispose();
			setUndecorated(true);
			setBounds(0, 0, getToolkit().getScreenSize().width, getToolkit().getScreenSize().height);
			setVisible(true);
			ceoEkran = true;
		}
		else {
			setVisible(true);
			setBounds(prethodniX, prethodniY, prethodnaS, prethodnaV);
			dispose();
			setUndecorated(false);
			setVisible(true);
			ceoEkran = false;
			pack();
		}
	}
	
	protected void prikazi(String s) {
		CardLayout cl = (CardLayout)(Interfejs.kartice.getLayout());
		cl.show(Interfejs.kartice, s);
	}

}
