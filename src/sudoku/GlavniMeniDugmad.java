package sudoku;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class GlavniMeniDugmad extends JPanel {

	/**
	 * Create the panel.
	 */
	public GlavniMeniDugmad() {
				
		JButton btnNovaIgra = new JButton("Nova igra");
		btnNovaIgra.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnNovaIgra.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Interfejs.kartice.add(new Odabir(), Interfejs.ODABIR);
				Interfejs.frame.prikazi(Interfejs.ODABIR);
			}
		});
		
		JButton btnIzadji = new JButton("Izadji");
		btnIzadji.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnIzadji.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		
		JButton btnOpcije = new JButton("Opcije");
		btnOpcije.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnOpcije.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Interfejs.kartice.add(new Opcije(), Interfejs.OPCIJE);
				Interfejs.frame.prikazi(Interfejs.OPCIJE);
			}
		});
		
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(121)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnIzadji, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addGap(121))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnOpcije, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addGap(121))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnNovaIgra, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE)
							.addContainerGap(121, Short.MAX_VALUE))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(btnNovaIgra, GroupLayout.PREFERRED_SIZE, 152, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnOpcije, GroupLayout.PREFERRED_SIZE, 152, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnIzadji, GroupLayout.PREFERRED_SIZE, 152, GroupLayout.PREFERRED_SIZE)
					.addGap(61))
		);
		setLayout(groupLayout);
		setOpaque(false);
	}

}
